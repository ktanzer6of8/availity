package com.tanzer6of8.availity03;

import java.util.ArrayDeque;
import java.util.Deque;

public class MatchParens {
	public boolean isNested(String inString) {
		boolean result = false;
		try {
			if (inString.isEmpty() || inString.isBlank()) {
				result = true;
			} else {
				Deque<Character> stack = new ArrayDeque<Character>();
				
				for(int i = 0; i < inString.length(); i++) {
					Character myChar = inString.charAt(i);
					switch (myChar) {
					case '(': 
						stack.push(myChar);
						break;
					case ')': 
						 if(stack.isEmpty()) {
							 return false;
						 }
						 stack.pop();
						 break;
					default: 
						//do nothing
					}
				}
				result = stack.isEmpty();
			}
			
		} catch (Exception e) {
			// TODO setup logging
			System.out.println("caught Exception " + e.toString());
			throw e;
		}
		return result;
	}
}
