package com.tanzer6of8.availity03;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.junit.Test;

public class AppTest 
{

	@Test
	 public void test_isNested_Empty() {
		MatchParens matchParens = new MatchParens();
		boolean result = matchParens.isNested("");
		assertTrue(result);
	}

	@Test
	public void test_isNested_Blank() {
		MatchParens matchParens = new MatchParens();
		boolean result = matchParens.isNested("   ");
		assertTrue(result);
	}

	@Test(expected=java.lang.NullPointerException.class)
	public void test_isNested_Null() {
		MatchParens matchParens = new MatchParens();
		boolean result = matchParens.isNested(null);
	}
	
	@Test
	public void test_isNested_simple_pass() {
		String t = "((()))";
		MatchParens matchParens = new MatchParens();
		assertTrue(matchParens.isNested(t));
	}
	
	@Test
	public void test_isNested_simple_fail() {
		String t = "((())";
		MatchParens matchParens = new MatchParens();
		assertFalse(matchParens.isNested(t));
	}

	@Test
	public void test_isNested_to_many_closing_fail() {
		String t = "(()))";
		MatchParens matchParens = new MatchParens();
		assertFalse(matchParens.isNested(t));
	}

	@Test
	public void test_isNested_complex_pass() {
		String t = "(stuff (stuff (stuff = true) more stuff) other stuff)";
		MatchParens matchParens = new MatchParens();
		assertTrue(matchParens.isNested(t));
	}

	@Test
	public void test_isNested_complex_fail() {
		// missing the closing paren
		String t = "(stuff (stuff (stuff = true) more stuff) other stuff";
		MatchParens matchParens = new MatchParens();
		assertFalse(matchParens.isNested(t));
	}
	
	@Test
	public void test_isNested_samplecode_pass() throws IOException {
		InputStream is = this.getClass().getClassLoader().getResourceAsStream("test01.lsp");
		String t = new String(is.readAllBytes(), StandardCharsets.UTF_8);
		MatchParens matchParens = new MatchParens();
		assertTrue(matchParens.isNested(t));
	}
	
	@Test
	public void test_isNested_samplecode_fail() throws IOException {
		// removed a single closing paren
		InputStream is = this.getClass().getClassLoader().getResourceAsStream("test01_bad.lsp");
		String t = new String(is.readAllBytes(), StandardCharsets.UTF_8);
		MatchParens matchParens = new MatchParens();
		assertFalse(matchParens.isNested(t));
	}
	
}
