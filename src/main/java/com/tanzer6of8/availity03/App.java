package com.tanzer6of8.availity03;

import java.util.Scanner;

public class App 
{
    public static void main( String[] args )
    {
	    MatchParens matchParens = new MatchParens();
    	Scanner scanner = null;
	    try {
	    	scanner = new Scanner(System.in);
		    System.out.print("Enter your string ? ");
		    String strLisp = scanner.next();
		    
		    boolean result = matchParens.isNested(strLisp);
		    System.out.print(result);
	    } finally {
	    	scanner.close();
	    }
    }
}
